<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <title>ChatLogin</title>
</head>
<body>
<div class="container text-center">
    <h2>Чат - вход</h2>
    <div class="center-block mx-auto" style="width: 50%; max-width: 360px; min-width: 200px;">
        <form method="post">
            <h5>Логин</h5>
            <input type="text" class="form-control" name="login" placeholder="Логин"
                   value="<?php if (isset($login)) echo $login; ?>">
            <h5>Пароль</h5>
            <input type="password" class="form-control" name="password" placeholder="Пароль">
            <div class="button-container mt-3">
                <a href="/" class="btn btn-primary" role="button">Отмена</a>
                <input type="submit" class="btn btn-light" name="submit" value="Ок">
            </div>
        </form>
    </div>
    <div class="error-container mt-3 mx-auto" style="width: 50%; max-width: 360px; min-width: 200px;">
        <?php if (is_array($errors)): ?>
            <?php foreach ($errors as $error): ?>
                <div class="alert alert-danger mb-0" role="alert" >
                    <?php echo $error; ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
</body>
</html>