<?php

class Db {
    /**
     * Метод получения доступа к БД
     * @return PDO
     */
    public static function getConnection(){
        $paramPath = ROOT . '/config/db_params.php';
        $params = include($paramPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']};charset=utf8";
        $db = new PDO($dsn, $params['user'], $params['password']);

        return $db;
    }
}