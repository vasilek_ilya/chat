<?php
/**
 * Функция автоподключения используемых файлов
 * из указанных директорий
 */
spl_autoload_register( function ($className){
    $arrayPath = array(
        '/models/',
        '/components/'
    );

    foreach ($arrayPath as $path){
        $path = ROOT . $path . $className . '.php';
        if (is_file($path)) {
            include_once ($path);
        }
    }
});