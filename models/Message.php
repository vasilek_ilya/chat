<?php

class Message {
    const SHOW_BY_DEFAULT = 15;

    public static function getMessages($count = self::SHOW_BY_DEFAULT){
        $db = Db::getConnection();

        $sql = "SELECT u.login as login, m.message as message FROM users u, messages m 
                WHERE u.id = m.user_id ORDER BY m.id DESC LIMIT :count;";

        $result = $db->prepare($sql);
        $result->bindParam(':count',$count, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $messages = array();
        $i = 0;
        while ($row = $result->fetch()){
            $messages[$i]['login'] = $row['login'];
            $messages[$i]['message'] = $row['message'];
            if ($_SESSION['user'] != $row['login']) {
                $messages[$i]['status'] = 'info';
            } else {
                $messages[$i]['status'] = 'success';
            }
            $i++;
        }

        return $messages;
    }

    public static function addMessage($message, $login) {
        $db = Db::getConnection();
        $id = User::getIdByLogin($login);

        $sql = 'INSERT INTO messages (user_id, message) VALUES (:id, :message);';

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':message', $message, PDO::PARAM_STR);

        return $result->execute();
    }
}