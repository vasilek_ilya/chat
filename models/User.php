<?php

class User {

    /**
     * Функции проверяющая есть пользователь с таким логином или нет
     * @param $login
     * @return bool
     */
    public static function checkLogin($login) {
        $db = Db::getConnection();

        $sql = 'SELECT COUNT(login) as count FROM users WHERE login = :login;';

        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $query = $result->fetch();

        if ($query['count'] > 0) return true;
        else return false;
    }

    /**
     * Функция проверяющая соответствие логина и пароля
     * @param $login
     * @param $password
     * @return bool
     */
    public static function checkPassword($login, $password) {
        $db = Db::getConnection();

        $sql = 'SELECT password as pas FROM users WHERE login = :login';

        $result = $db->prepare($sql);
        $result->bindParam(':login',$login, PDO::PARAM_STR);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        $query = $result->fetch();

        return password_verify($password, $query['pas']);
    }

    /**
     * Функция добавляющая нового пользователя
     * @param $login
     * @param $password
     * @return bool
     */
    public static function addUser($login, $password) {
        $db = Db::getConnection();

        $sql = 'INSERT INTO users (login, password)
                VALUES (:login, :password);';

        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);

        return $result->execute();
    }


    public static function getIdByLogin($login) {
        $db = Db::getConnection();

        $sql = 'SELECT id FROM users WHERE login = :login';

        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);

        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();
        $query = $result->fetch();

        return $query['id'];
    }
}