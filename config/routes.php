<?php

/**
 * Массив соответсвий запросов
 * и нужных им котроллеров и экшенов
 */
return array(
    'getMessage' => 'chat/getMessage',  //ChatController actionGetMessage
    'messenger' => 'chat/messenger',    //ChatController actionMessenger
    'signUp' => 'chat/signUp',          //ChatController actionSignUp
    'login' => 'chat/login',            //ChatController actionLogin
    'logout' => 'chat/logout',          //ChatController actionLogout
    '' => 'chat/index',                 //ChatController actionIndex
);